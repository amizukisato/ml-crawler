let config = {
  database: {
    name: "MercadoLivreCrawler",
    mongoUrl: "localhost:22222",
    modelUrl: "./Models/models.json"
  },
  server: {
    appName: "ML Crawller",
    port: 3000
  }
};
module.exports = config;
