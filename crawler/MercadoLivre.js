const fs = require("fs");
var JSSoup = require("jssoup").default;
var request = require("request-promise");

class MercadoLivre {
  constructor(query, limit, skip) {
    this.query = query;
    this.limit = limit;
    this.skip = skip;
    this.urlBase = "https://lista.mercadolivre.com.br/";
  }

  async loadSoup() {
    let url = `${this.urlBase}${this.query}`;
    let htmlString = await request({
      uri: url
    });
    return new JSSoup(htmlString.toString());
  }

  // async loadSoup() {
  //   let htmlString = fs.readFileSync("./crawler/example.html");
  //   return new JSSoup(htmlString.toString());
  // }

  async parse() {
    let soup = await this.loadSoup();
    let tags = soup.findAll();

    let data = [];
    let product = { price: 0 };

    tags.forEach(e => {
      if (data.length <= this.limit) {
        if (e.attrs.class == "main-title") {
          let name = e && e.text ? e.text.toString() : "";
          product.name = name.trim();
        }

        if (e.attrs.class == "item__brand-title-tos") {
          let shopName = e && e.text ? e.text.toString() : "";
          product.store = shopName.trim();
        }

        if (e.attrs.class == "item__condition") {
          let state = e && e.text ? e.text.toString() : "";
          product.state = state.trim();
        }

        if (e.attrs.class == "images-viewer") {
          product.link = e.attrs["item-url"];
        }
        if (e.attrs.class == "price__fraction") {
          let priceFraction = e && e.text ? e.text.toString() : "";
          product.price = Number(priceFraction);
        }
        if (e.attrs.class == "price__decimals") {
          let priceDecimals = e && e.text ? e.text.toString() : "";
          product.price = product.price + Number(priceDecimals) / 100;
          if (product.price > 0) {
            data.push(product);
          }
          product = { price: 0 };
        }
      }
    });

    return data;
  }
}

module.exports = MercadoLivre;
