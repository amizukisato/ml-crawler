const express = require("express");
const config = require("./config/config.development");
let app = express();
let factoryController = require("./controller/FactoryController");

app.use(express.urlencoded());
app.use(express.json());

app.get("/", function(req, res) {
  res.send("Hello World!");
});

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

factoryController.load(app);

app.listen(config.server.port, function() {
  console.log(
    `${config.server.appName} listening on port ${config.server.port}!`
  );
});
