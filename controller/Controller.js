let router = require("express").Router();
let DAO = require("./../database/DAO");
let dao = new DAO();

class Controller {
  constructor() {
    router.get("/", this.get);
    router.put("/", this.put);
    router.post("/", this.post);
    router.delete("/", this.delete);
  }

  getRouter() {
    return router;
  }

  get(req, res) {
    res.send(dao.get());
  }
  put(req, res) {}
  post(req, res) {}
  delete(req, res) {}
}

module.exports = Controller;
