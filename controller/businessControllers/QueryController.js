const Controller = require("../Controller");
const MercadoLivre = require("../../crawler/MercadoLivre");
let router = require("express").Router();

class QueryController extends Controller {
  constructor() {
    super();
    router.post("/crawler", this.createCrawler);
  }

  async createCrawler(req, res) {
    let queryParams = req.body;
    let skip = queryParams.skip ? queryParams.skip : 0;

    let mercadoLivreCrawler = new MercadoLivre(
      queryParams.query,
      queryParams.limit,
      skip
    );
    let jsonParsed = await mercadoLivreCrawler.parse();
    res.send(jsonParsed);
  }
  getRouter() {
    return router;
  }
}

module.exports = QueryController;
