const config = require("../config/config.development");
const fs = require("fs");

const controllers = {
  query: require("./businessControllers/QueryController"),
  default: require("./Controller")
};

class FactoryController {
  constructor() {
    this.modelUrl = config.database.modelUrl;
    this.data = JSON.parse(fs.readFileSync(this.modelUrl));
  }

  load(app) {
    this.data.forEach(table => {
      let tableName = table.table.toLowerCase();
      let endPoint = `/${tableName}`;
      let Controller =
        tableName in controllers ? controllers[tableName] : controllers.default;
      let controller = new Controller();
      app.use(endPoint, controller.getRouter());
    });
  }
}

module.exports = new FactoryController();
